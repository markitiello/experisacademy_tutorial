find_package(rostest REQUIRED)


## Add gtest based cpp test target and link libraries


include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)


catkin_add_gtest(gtest_example_test   ${CMAKE_CURRENT_SOURCE_DIR}/test/unit_test_gtest/gtest_example.cpp )
target_link_libraries(gtest_example_test ${catkin_LIBRARIES})


 

