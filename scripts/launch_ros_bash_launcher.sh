#acquiring GIT main dir
GIT_MAIN_DIR="$(git rev-parse --show-toplevel)"
cd $GIT_MAIN_DIR


#setup ros enviroment 
source devel/setup.bash

#launching building process
catkin_make

#launch base launch
roslaunch test_package base.launch
